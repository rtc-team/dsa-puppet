# Our onion balance for v3 onions
class onion::balancev3(
  String                 $puppet_facts_onionbalancev3_file,
) {
  include onion

  package { 'onionbalance':
    ensure => installed,
  }
  service { 'onionbalance':
    ensure  => running,
    require => Package['onionbalance'],
  }

  file { '/etc/onionbalance/configs':
    ensure  => directory,
    require => Package['onionbalance'],
  }

  file { '/usr/local/bin/create-onionbalancev3-config':
    mode   => '0555',
    source => 'puppet:///modules/onion/create-onionbalancev3-config',
    notify => Exec['create-onionbalancev3-config'],
  }

  concat::fragment { 'onion::torrc_control_header':
    target  => '/etc/tor/torrc',
    order   => '10',
    content => "ControlPort 9051\n\n",
  }

  @@concat::fragment { "onion::balance::${::hostname}::onionbalancev3-services.yaml":
    target  => $puppet_facts_onionbalancev3_file,
    content => "${::onionv3_balance_service_hostname}\n",
    tag     => 'onionbalancev3-services.yaml',
  }

  concat { '/etc/onionbalance/config-dsa-snippetv3.yaml':
    notify  => Exec['create-onionbalancev3-config'],
    require => File['/usr/local/bin/create-onionbalancev3-config']
  }
  Concat::Fragment <<| tag == 'onion::balance::dsa-snippetv3' |>>

  exec { 'create-onionbalancev3-config':
    command     => '/usr/local/bin/create-onionbalancev3-config',
    refreshonly => true,
    require     => [ File['/usr/local/bin/create-onionbalancev3-config'], Package['onionbalance'] ],
    notify      => Service['onionbalance'],
  }

  mon::service { 'procs/onionbalance':
    vars => {
      argument => 'onionbalance',
      command  => 'onionbalance',
      user     => 'onionbalance',
      warning  => '1:1',
      critical => '1:',
    }
  }
}
