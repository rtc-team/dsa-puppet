# Please contact the RTC team about this service at debian-rtc-team@alioth-lists.debian.net
#

class profile::prosody {

  include profile::prosody::packages

  # get the users in the debvoip group from a fact, append @debian.org and save it as $admins
  $admins = $facts['local_groups']['debvoip']['mem'].map |$x| { "${x}@debian.org" }

  class { 'prosody':
    user              => 'prosody',
    group             => 'prosody',
    use_libevent      => false,
    daemonize         => true,
    s2s_secure_auth   => false,
    package_name      => 'prosody-modules',
    ssl_custom_config => false,
    admins            => $admins,
    log_sinks         => [],
    log_advanced      => {
      'error' => 'syslog',
    },
    authentication    => 'ha1',
    custom_options    => {
      'reload_modules'    => [ 'firewall' ],
      'auth_ha1_file'     => '/var/local/rtc-passwords.prosody',
      'auth_ha1_use_ha1b' => true,
      'auth_ha1_realm'    => 'rtc.debian.org',
      'firewall_scripts'  => '/srv/prosody/antispam/spammer.pfw',
      'contact_info'      =>  {
        abuse    => ['mailto:abuse-rtc@debian.org'],
        security => ['mailto:abuse-rtc@debian.org'],
        feedback => ['mailto:abuse-rtc@debian.org'],
      },
      'https_ssl'         => {
        'key'         => '/etc/ssl/private/xmpp.debian.org.key',
        'certificate' => '/etc/ssl/debian/certs/xmpp.debian.org.crt-chained',
      },
      'https_certificate' => {
        '[5281]'         => '/etc/ssl/private/xmpp.debian.org.key',
      },
    },
    # we override whatever the module decides as a base
    modules_base      => [
      'roster', 'saslauth', 'tls', 'dialback', 'disco', 'posix', 'private',
      'vcard', 'version', 'uptime', 'time', 'ping', 'pep', 'register',
    ],
    # and add the modules we want on top
    modules           => [
      'admin_adhoc',
      'blocklist',
      'carbons', 'carbons_adhoc',
      'cloud_notify', 'csi', 'filter_chatstates', 'http', 'firewall',
      'http_upload', 'mam', 'smacks', 'throttle_presence',
      'server_contact_info',
      'vcard4',
      'vcard_legacy',
    ],
  }

  prosody::virtualhost {
    'debian.org':
      ensure         => present,
      ssl_key        => '/etc/ssl/private/xmpp.debian.org.key',
      ssl_cert       => '/etc/ssl/debian/certs/xmpp.debian.org.crt-chained',
      ssl_copy       => false,
      components     => {
        'conference.debian.org' => {
          'type' => 'muc',
        },
        'xmpp.debian.org'       => {
          'type' => 'http_upload',
        },
      },
      custom_options => {
        'http_upload_quota'           => 100*1024*1024,  # 100M
        'http_upload_expire_after'    => 60*60*24*30,  # 30 days
        'http_upload_file_size_limit' => 10*1024*1024,  # 10M
      },
      disco_items    => ['xmpp.debian.org'],
  }

  $prosody_directories = [ '/srv/prosody', '/srv/prosody/antispam' ]

  $prosody_directories.each |$index, $directory| {
    file { $directory:
      ensure => directory,
      owner  => 'root',
      group  => 'debvoip',
      mode   => '2775',
    }
  }

}
