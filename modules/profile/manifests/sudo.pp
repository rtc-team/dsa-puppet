# Debian.org's sudo setup
class profile::sudo {
  ensure_packages ( [
    'libpam-pwdfile',
  ], {
    ensure => 'installed',
  })

  file { '/etc/pam.d/sudo':
    source  => 'puppet:///modules/profile/sudo/pam',
    require => Package['sudo'],
  }

  class { 'sudo':
    content => 'profile/sudo/sudoers',
  }

  sudo::conf { 'dsa-main':
    priority => 5,
    content  => template('profile/sudo/dsa-main'),
  }
}
