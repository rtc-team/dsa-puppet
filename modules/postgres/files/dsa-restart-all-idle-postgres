#! /bin/bash

# Written by Peter Palfrader
# Based on code by Tollef Fog Heen <tfheen@err.no> based on code by Peter Palfrader
#
# Restart all postgresql clusters that show up in dsa-check-libs when they do
# not have connections to them.

set -e
set -u

# chk_net <port>
# returns true (0) if there are connections to that port.
chk_net() {
  local port="$1"; shift
  local con="$(ss -H -nt "sport = :$port" | wc -l)"
  if [ "$con" -ge 1 ]; then
    return 0
  else
    return 1
  fi
}

get_pidfile_value() {
  local version="$1"; shift
  local name="$1"; shift

  local fn="/run/postgresql/${version}-${name}.pid"
  if [ -e "$fn" ] ; then
    cat "$fn"
  else
    echo "No pidfile at $fn" >&2
    exit 1
  fi
}

handle_cluster() {
  local version="$1"; shift
  local name="$1"; shift
  local port="$1"; shift

  pid="$(get_pidfile_value "$version" "$name")"
  while chk_net "$port" && [ "$pid" = "$(get_pidfile_value "$version" "$name")" ]; do
    sleep 60
  done
  systemctl restart "postgresql@${version}-${name}.service"
}

tmpfile="$(mktemp)"
trap "rm -f '$tmpfile'" EXIT
/usr/lib/nagios/plugins/dsa-check-libs > "$tmpfile" || true

pg_lsclusters -h | while read version name port status; do
  echo "$status" | grep -q --word-regexp online || continue
  pid="$(get_pidfile_value "$version" "$name")"
  grep -q --word-regexp $pid "$tmpfile" || continue

  handle_cluster $version $name $port
done
