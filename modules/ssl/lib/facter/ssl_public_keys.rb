Facter.add(:ssl_public_keys) do
  setcode do
    keys_hash = {}
    Dir.glob(File.join('/etc/ssl/private', '*.key')).each { |filepath|
      key_name = File.basename(filepath, '.key')
      begin
        if FileTest.file?(filepath)
          keys_hash[key_name] = %x{openssl rsa -in #{filepath} -pubout 2>/dev/null}
        end
      rescue
        puts "cannot read SSL key: " + key_name
      end
    }
    keys_hash
  end
end

