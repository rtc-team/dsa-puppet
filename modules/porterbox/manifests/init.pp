class porterbox {
	include schroot

	package { 'dgit':
		ensure => installed,
	}

	file { '/usr/local/bin/dd-schroot-cmd':
		mode    => '0555',
		source  => 'puppet:///modules/porterbox/dd-schroot-cmd',
	}
	file { '/usr/local/bin/schroot-list-sessions':
		mode    => '0555',
		source  => 'puppet:///modules/porterbox/schroot-list-sessions',
	}
	file { '/etc/cron.d/puppet-update-dchroots': ensure => absent; }
	concat::fragment { 'puppet-crontab--porterbox-chroot-update':
		target => '/etc/cron.d/puppet-crontab',
		content  => @(EOF)
			0 15 * * 0 root PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots
			| EOF
	}
	file { '/etc/cron.weekly/puppet-mail-big-homedirs':
		mode    => '0555',
		source  => 'puppet:///modules/porterbox/mail-big-homedirs',
	}

	if $::debarchitecture == 'arm64' {
		# Enable CP15, SETEND and SWP instructions emulation in aarch32
		# compat mode, needed to support armel binaries.
		base::sysctl { 'puppet-abi_cp15':
			key	 => 'abi.cp15',
			value => '1',
		}
		base::sysctl { 'puppet-abi_setend':
			key	 => 'abi.setend',
			value => '1',
		}
		base::sysctl { 'puppet-abi_swp':
			key	 => 'abi.swp',
			value => '1',
		}
	}
}
