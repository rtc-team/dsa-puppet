# = Class: apache2
#
# Standard apache config debian.org hosts
#
# == Sample Usage:
#
#   include apache2
#
# @param smaller_number_of_threads by default the worker config is geared towards
#                                  serving static/cheap content.  If the host is very
#                                  script heavy (say the bug tracking system), set this
#                                  to reduce the number of worker threads.
# @param rlimitnproc A resource limit for number of processes.  The default is usually fine.
# @param rlimitmem   A resource limit for memory usage.  The default is usually fine.
# @param public Whether this host's apache should be accessible from the public internet.
#               Sets appropriate firewall rules and optionally rate limits.
# @param mpm Which Multi-Processing Modules to use. Defaults to worker;
#            the alternative is prefork.
# @param rate_limit Rate limit incoming connections at the netfilter level.  If false,
#                   (and public is true), all incoming connections to the http
#                   and https ports get sent to the http chain, and accepted at
#                   ferm prio 90, so other things can be done to web traffic
#                   before that.
# @param tls13 If false, disable TLS 1.3 (e.g. to workaround incompatibility with optional client certs)
# @param bufferedlogs if false, do not buffer logs
class apache2(
  Boolean $smaller_number_of_threads = false,
  Integer $rlimitnproc = 256,
  Integer $rlimitmem = 192 * 1024 * 1024,
  Boolean $public = true,
  Enum['prefork','worker'] $mpm = 'worker',
  Boolean $rate_limit = false,
  Boolean $tls13 = true,
  Boolean $bufferedlogs = true,
) {
  include webserver

  package { 'apache2':
    ensure => installed,
  }

  service { 'apache2':
    ensure  => running,
    require => Package['apache2'],
  }

  apache2::module { 'reqtimeout': }
  apache2::module { 'info': }
  apache2::module { 'status': }
  apache2::module { 'headers': }
  apache2::module { 'macro': }
  apache2::module { 'http2': }

  apache2::site { '00-default':
    site    => 'default-debian.org',
    content => template('apache2/default-debian.org.erb'),
  }
  apache2::site { 'xx-default-ssl':
    site    => 'default-debian.org-ssl',
    content => template('apache2/default-debian.org-ssl.erb'),
  }

  apache2::site { '000-default':
    ensure => absent,
  }

  apache2::config { 'serve-cgi-bin':
    ensure => absent,
  }

  apache2::config { 'resource-limits':
    content => template('apache2/resource-limits.erb'),
  }

  apache2::config { 'security':
    source => 'puppet:///modules/apache2/security',
  }

  apache2::config { 'logformat-privacy':
    source => 'puppet:///modules/apache2/logformat-privacy',
  }

  apache2::config { 'local-serverinfo':
    source => 'puppet:///modules/apache2/local-serverinfo',
  }

  apache2::config { 'server-status':
    source => 'puppet:///modules/apache2/server-status',
  }

  apache2::config { 'puppet-ssl-macros':
    source => 'puppet:///modules/apache2/puppet-ssl-macros',
  }

  apache2::config { 'puppet-ftp-macros':
    source => 'puppet:///modules/apache2/puppet-ftp-macros',
  }

  apache2::config { 'puppet-config':
    content => template('apache2/puppet-config.erb'),
  }

  apache2::config { 'headers':
    source => 'puppet:///modules/apache2/headers',
  }

  apache2::config { 'disabled-service':
    source => 'puppet:///modules/apache2/disabled-service',
  }

  apache2::module { 'mpm_event': ensure => absent }
  apache2::module { 'mpm_worker' : ensure => ($mpm == 'worker' ) ? { true => 'present', default => absent } }
  apache2::module { 'mpm_prefork': ensure => ($mpm == 'prefork') ? { true => 'present', default => absent } }

  file { '/etc/apache2/mods-available/mpm_worker.conf':
    content => template('apache2/mpm_worker.erb'),
    require => Package['apache2'],
  }

  file { '/etc/logrotate.d/apache2':
    source => 'puppet:///modules/apache2/apache2.logrotate',
  }

  file { '/var/log/apache2':
    ensure => directory,
    mode   => '0755',
  }
  file { '/var/log/apache2/.nobackup':
    mode    => '0644',
    content => '',
  }

  file { '/usr/share/munin/plugins/apache_servers':
    source => 'puppet:///modules/apache2/munin/apache_servers',
    mode   => '0755',
  }
  munin::check { 'apache_accesses': }
  munin::check { 'apache_processes': }
  munin::check { 'apache_volume': }
  munin::check { 'apache_servers': }
  munin::check { 'ps_apache2':
    script => 'ps_',
  }
  # The munin script needs this
  package { 'libwww-perl':
    ensure => installed,
  }

  if $public {
    ferm::rule { 'dsa-http':
      prio        => '23',
      description => 'A web subchain',
      domain      => '(ip ip6)',
      rule        => 'proto tcp dport (http https 6081) jump http'
    }

    if $rate_limit {
      include apache2::dynamic
    } else {
      ferm::rule { 'dsa-http-allow':
        description => 'http subchain, allow everything',
        prio        => '90',
        chain       => 'http',
        domain      => '(ip ip6)',
        rule        => 'jump ACCEPT',
      }
    }
  }

  exec { 'service apache2 reload':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => 'service apache2 reload',
    refreshonly => true,
    require     =>  Package['apache2'],
  }

  apache2::config { 'puppet-ssl-key-pins':
    ensure => absent,
    notify => Exec['service apache2 reload'],
  }

  apache2::config { 'local-scheduled-shutdown':
    ensure => 'absent',
  }
}
