# add an exim submission domain entry on this host
#
# @param domain     email domain (defaults to $name)
define exim::submissiondomain (
  String $domain = $name,
) {
  concat::fragment { "submission_${domain}":
    target  => '/etc/exim4/submission-domains',
    content => $domain,
  }
}
