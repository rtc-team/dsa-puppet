# mail_tlsa class : creates the appropriate TLSA records in debian.org zone
# for servers listening on port 587
class exim::mail_tlsa {
  $autocertdir = hiera('paths.auto_certs_dir')
  dnsextras::tlsa_record{ 'tlsa-submission':
    zone     => 'debian.org',
    certfile => "${autocertdir}/${::fqdn}.crt",
    port     => 587,
    hostname => $trusted["certname"],
  }

  $le_dir = hiera('paths.letsencrypt_dir')
  $le_crt_fn = "${le_dir}/${::fqdn}.crt"
  if (file($le_crt_fn, '/dev/null') != '') {
    dnsextras::tlsa_record{ 'tlsa-submission-le':
      zone     => 'debian.org',
      certfile => $le_crt_fn,
      port     => 587,
      hostname => $trusted["certname"],
    }
  }
}
