# = Class: monit
#
# This class installs and configures monit
#
# == Sample Usage:
#
#   include monit
#
class monit {
	package { 'monit':
		ensure => purged
	}
	file { [ '/etc/monit/',
		 '/etc/monit/monit.d',
		 '/etc/monit/monit.d/01puppet',
		 '/etc/monit/monit.d/00debian.org'
		]:
		ensure  => absent,
		force   => true
	}
}
