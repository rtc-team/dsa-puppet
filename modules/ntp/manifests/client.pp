class ntp::client {
	file { '/etc/default/ntp':
		source => 'puppet:///modules/ntp/etc-default-ntp',
		require => Package['ntp'],
		notify  => Service['ntp']
	}
	file { '/etc/ntp.keys.d/ntpkey_iff_conova-node03':
		source => 'puppet:///modules/ntp/ntpkey_iff_conova-node03.pub',
		notify => Service['ntp'],
	}
	file { '/etc/ntp.keys.d/ntpkey_iff_conova-node04':
		source => 'puppet:///modules/ntp/ntpkey_iff_conova-node04.pub',
		notify => Service['ntp'],
	}
	file { '/etc/ntp.keys.d/ntpkey_iff_manda-node03':
		source => 'puppet:///modules/ntp/ntpkey_iff_manda-node03.pub',
		notify => Service['ntp'],
	}
	file { '/etc/ntp.keys.d/ntpkey_iff_manda-node04':
		source => 'puppet:///modules/ntp/ntpkey_iff_manda-node04.pub',
		notify => Service['ntp'],
	}
	file { '/usr/local/sbin/ntp-restart-if-required':
		source => 'puppet:///modules/ntp/ntp-restart-if-required',
		mode    => '0555',
	}
}
