# fail2ban setup
#
# Checks that are only used on particular systems are defined elsewhere,
# generally in subclasses.
class fail2ban {
	package { 'fail2ban':
		ensure => installed,
	}

	service { 'fail2ban':
		ensure  => running,
	}

	file { '/etc/fail2ban/jail.d/dsa-00-default.conf':
		source => 'puppet:///modules/fail2ban/jail/dsa-00-default.conf',
		notify => Service['fail2ban'],
	}

	ferm::conf { 'f2b':
		content  => @(EOF),
				@hook post  "type fail2ban-client > /dev/null && (fail2ban-client ping > /dev/null && fail2ban-client reload > /dev/null ) || true";
				@hook flush "type fail2ban-client > /dev/null && (fail2ban-client ping > /dev/null && fail2ban-client reload > /dev/null ) || true";
				| EOF
	}

	ferm::rule { 'dsa-f2b-setup1':
		prio        => '005',
		description => 'f2b master rule',
		chain       => 'dsa-f2b',
		domain      => '(ip ip6)',
		rule        => '',
		notarule    => true,
	}
	ferm::rule { 'dsa-f2b-setup2':
		prio        => '005',
		description => 'f2b master rule',
		chain       => 'INPUT',
		domain      => '(ip ip6)',
		rule        => 'jump dsa-f2b',
	}

	# XXX Maybe this will be automatically done in buster, it is certainly needed in stretch. So maybe:  versioncmp($::lsbmajdistrelease, '9') <= 0
	concat::fragment { 'puppet-crontab--fail2ban-cleanup':
		target  => '/etc/cron.d/puppet-crontab',
		content => @(EOF)
			17 * * * * root chronic python3 -c "import sys, logging; logging.basicConfig(stream=sys.stdout, level=logging.INFO); from fail2ban.server.database import Fail2BanDb; db = Fail2BanDb('/var/lib/fail2ban/fail2ban.sqlite3'); db.purge(); db._db.cursor().execute('VACUUM')"
			| EOF
	}

}
