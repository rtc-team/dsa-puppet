class ganeti_instance_debootstrap {
  ensure_packages ( [
    'ganeti-instance-debootstrap',
    'passwdqc',
    'ipv6calc',
  ], {
    ensure => 'installed',
  } )

  file { '/etc/default/ganeti-instance-debootstrap':
    source  => 'puppet:///modules/ganeti_instance_debootstrap/etc-default-ganeti-instance-debootstrap',
  }
  file { '/etc/ganeti/instance-debootstrap':
    ensure  => directory,
    recurse => true,
    purge   => true,
    source  => 'puppet:///modules/ganeti_instance_debootstrap/etc-ganeti-instance-debootstrap',
  }
  file { '/etc/ganeti/instance-debootstrap/hooks':
    ensure  => directory,
    recurse => true,
    purge   => true,
    source  => 'puppet:///modules/ganeti_instance_debootstrap/etc-ganeti-instance-debootstrap/hooks',
    mode    => '0555',
  }
}
