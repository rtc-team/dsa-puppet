#!/bin/sh

mkswapfile() {
    dd if=/dev/zero of="$TARGET"/swapfile bs=1024K count=512
    mkswap "$TARGET"/swapfile
    chmod 0 "$TARGET"/swapfile
    echo '/swapfile none swap sw 0 0' >> "$TARGET/etc/fstab"
    exit 0
}

if [ "$DISK_COUNT" -lt 2 ] ; then
    echo "Only one disk found, creating a 512M /swapfile instead" >&2
    mkswapfile
fi

# Make sure we're not working on the root directory
if [ -z "$TARGET" ] || [ "$TARGET" = "/" ]; then
    echo "Invalid target directory '$TARGET', aborting." 1>&2
    exit 1
fi

if [ "$(mountpoint -d /)" = "$(mountpoint -d "$TARGET")" ]; then
    echo "The target directory seems to be the root dir, aborting."  1>&2
    exit 1
fi

if [ -f /sbin/blkid ] && [ -x /sbin/blkid ]; then
  VOL_ID="/sbin/blkid -o value -s UUID"
else
  for dir in /lib/udev /sbin; do
    if [ -f $dir/vol_id ] && [ -x $dir/vol_id ]; then
      VOL_ID="$dir/vol_id -u"
    fi
  done
fi

if [ -z "$VOL_ID" ]; then
  log_error "vol_id or blkid not found, please install udev or util-linux"
  exit 1
fi

for i in $(seq 1 "$DISK_COUNT"); do
    swapdev=$(eval "echo \$DISK_${i}_PATH")
    devname=$(eval "echo \$DISK_${i}_NAME")
    if [ -z "$swapdev" ]; then
        continue
    fi
    if [ -z "$devname" ] || [ "$devname" != "swap" ]; then
        echo "Not using disk $i ($swapdev) because it is not named 'swap' (name: $devname)" 1>&2
        continue
    fi

    echo "Using disk $swapdev as swap..." 1>&2

    mkswap "$swapdev"
    swap_uuid=$($VOL_ID "$swapdev" || true )

    [ -n "$swap_uuid" ] && cat >> "$TARGET/etc/fstab" <<EOF
UUID=$swap_uuid   swap            swap    defaults        0       0
EOF
done

if ! grep -q swap "$TARGET/etc/fstab"; then
    echo "No disk named 'swap' declared, creating /swapfile" 1>&2
    mkswapfile
fi
