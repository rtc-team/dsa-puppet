define stunnel4::client($accept, $connecthost, $connectport) {

	include stunnel4

	file { "/etc/stunnel/puppet-${name}-peer.pem":
		content => inline_template( @("EOF"),
						<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/" + @connecthost + ".crt") %>
						<%= File.read(scope().call_function("hiera", ["paths.auto_certs_dir"]) + "/ca.crt") %>
						| EOF
					),
		notify  => Exec["restart_stunnel_${name}"],
	}

	stunnel4::generic { $name:
		client  => true,
		verify  => 3,
		cafile  => "/etc/stunnel/puppet-${name}-peer.pem",
		accept  => $accept,
		connect => "${connecthost}:${connectport}",
	}
}

