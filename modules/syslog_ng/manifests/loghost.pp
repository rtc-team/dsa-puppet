class syslog_ng::loghost {
	file { '/etc/logrotate.d/syslog-ng-loggers':
		source  => 'puppet:///modules/syslog_ng/syslog-ng.logrotate.loggers',
		require => Package['syslog-ng']
	}
	file { '/etc/cron.daily/puppet-handle-loghost-logs':
		source => 'puppet:///modules/syslog_ng/loggers-cron',
		mode   => '0555',
	}
	file { '/var/log/.nobackup':
		ensure => present,
	}
}
