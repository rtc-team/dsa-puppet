class stretch::network_online {
	if versioncmp($::lsbmajdistrelease, '9') <= 0 {
		
		exec { 'systemctl enable ifupdown-wait-online.service':
			refreshonly => true,
		}

		file { '/lib/ifupdown/wait-online.sh':
			mode   => '0555',
			source => 'puppet:///modules/stretch/wait-online.sh',
		}
		file { '/lib/systemd/system/ifupdown-wait-online.service':
			mode   => '0555',
			source => 'puppet:///modules/stretch/ifupdown-wait-online.service',
			notify  => Exec['systemctl enable ifupdown-wait-online.service'],
		}
	}
}

