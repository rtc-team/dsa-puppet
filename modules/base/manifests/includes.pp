class base::includes {
  include roles::mta

  include munin
  include syslog_ng
  include profile::sudo
  include ssh
  include debian_org
  include time
  include ssl
  include hardware
  include nagios::client
  include resolv
  include roles
  include motd
  include unbound
  include bacula::client
  include grub
  include multipath
  include popcon
  include portforwarder
  include postgres
  include haveged
  include tcp_bbr
  include certregen::client
}
