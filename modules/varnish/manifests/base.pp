class varnish::base {
	package { 'varnish':
		ensure => installed,
	}
	service { 'varnish':
		ensure => running,
	}

	file { '/var/lib/varnish/.nobackup':
		ensure  => present,
		content => "",
		require =>  Package['varnish'],
		mode    => '0444',
	}
}

