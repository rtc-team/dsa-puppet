# ftpmaster role
#
# @param db_port    port of the database cluster for ftp-master's dak
class roles::ftp_master (
  Integer $db_port,
)
{
  include roles::dakmaster
  include roles::signing
  include roles::historical_master
  include apache2

  rsync::site { 'dakmaster':
    source      => 'puppet:///modules/roles/dakmaster/rsyncd.conf',
    # Needs to be at least number of direct mirrors plus some spare
    max_clients => 50,
    sslname     => 'ftp-master.debian.org',
  }

  ssl::service { 'ftp-master.debian.org':
    notify   => Exec['service apache2 reload'],
    key      => true,
    tlsaport => [443, 1873],
  }

  # export ssh allow rules for hosts that we should be able to access
  @@ferm::rule::simple { "dsa-ssh-from-ftp_master-${::fqdn}":
    tag         => 'ssh::server::from::ftp_master',
    description => 'Allow ssh access from ftp_master',
    chain       => 'ssh',
    saddr       => $base::public_addresses,
  }

  postgres::cluster::hba_entry { 'dak-guest':
    pg_port  => $db_port,
    database => 'projectb',
    user     => 'guest',
    address  => '127.0.0.1',
    method   => 'trust',
    order    => '20',
    firewall => false,
  }

  exim::dkimdomain { 'ftp-master.debian.org': }
}
