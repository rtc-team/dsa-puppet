# wanna-build guest access to DB
#
# @param db_address       hostname of the postgres server for this service
# @param db_port          port of the postgres server for this service
# @param database         list of databases to give access to
# @param address          hosts to give access
# @param connection_type  connection type
class roles::buildd_master::db_guest_access (
  String  $db_address     = $roles::buildd_master::params::db_address,
  Integer $db_port        = $roles::buildd_master::params::db_port,
  Array[String] $database = ['wanna-build'],
  Enum['local', 'host', 'hostssl'] $connection_type = 'hostssl',
  Optional[Variant[Stdlib::IP::Address, Array[Stdlib::IP::Address]]] $address = $base::public_addresses,
) inherits roles::buildd_master::params {
  @@postgres::cluster::hba_entry { "buildd_master-guest-${::fqdn}":
    tag             => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port         => $db_port,
    database        => $database,
    user            => 'guest',
    address         => $address,
    connection_type => $connection_type,
    method          => 'trust',
    order           => '25',
  }
}
