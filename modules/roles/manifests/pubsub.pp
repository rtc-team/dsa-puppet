class roles::pubsub {
  include roles::pubsub::params
  include roles::pubsub::entities

  $cluster_cookie  = $roles::pubsub::params::cluster_cookie

  # Get the fact named hostname from all nodes in puppetdb with class Roles::Pubsub
  $query = 'facts { name = "hostname" and resources { type = "Class" and title = "Roles::Pubsub" } }'
  $cluster_nodes = sort(puppetdb_query($query).map |$value| { $value["value"] })

  class { 'rabbitmq':
    config_cluster    => true,
    cluster_nodes     => $cluster_nodes,
    cluster_node_type => 'disc',
    erlang_cookie     => '8r17so6o1s124ns49sr08n0o24342160',
    delete_guest_user => true,
    ssl               => true,
    ssl_cacert        => '/etc/ssl/debian/certs/ca.crt',
    ssl_cert          => '/etc/ssl/debian/certs/thishost-server.crt',
    ssl_key           => '/etc/ssl/private/thishost-server.key',
    ssl_port          => 5671,
    ssl_verify        => 'verify_none',
    repos_ensure      => false,
  }

  user { 'rabbitmq':
    groups => 'ssl-cert'
  }

  ferm::rule { 'rabbitmq':
    description => 'rabbitmq connections',
    domain      => '(ip ip6)',
    rule        => '&SERVICE_RANGE(tcp, 5671, $HOST_DEBIAN)'
  }

  @@ferm::rule::simple { "pubsub-cluster-from-${::fqdn}":
    tag   => 'roles::pubsub::intra-cluster',
    saddr => $base::public_addresses,
  }
  Ferm::Rule::Simple <<| tag == 'roles::pubsub::intra-cluster' |>>
}
