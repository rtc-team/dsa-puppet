class roles::l10n {
  include apache2
  ssl::service { 'l10n.debian.org': notify  => Exec['service apache2 reload'], key => true, }
}
