# a web mirror for bugs.debian.org
class roles::bugs_mirror {
  include roles::bugs_web

  rsync::site { 'bugs_mirror':
    source      => 'puppet:///modules/roles/bugs_mirror/rsyncd.conf',
    max_clients => 100,
  }

  apache2::site { '009-bugs-mirror.debian.org':
    site   => 'bugs-mirror.debian.org',
    source => 'puppet:///modules/roles/bugs_mirror/bugs-mirror.debian.org',
  }

  file { '/srv/bugs-webmirror.debian.org':
    ensure => directory,
    owner  => 'debbugs',
    group  => 'debbugs',
    mode   => '2755',
  }
  file { '/srv/bugs.debian.org':
    ensure => link,
    target => 'bugs-webmirror.debian.org',
  }

  file { '/srv/bugs-webmirror.debian.org/.nobackup':
    ensure => present,
    owner  => 'debbugs',
    group  => 'debbugs',
  }

  file { '/usr/local/bin/bugs-ssh-command-wrap':
    source => 'puppet:///modules/roles/bugs_mirror/bugs-ssh-command-wrap',
    mode   => '0555',
  }

  ssh::authorized_key_collect { 'bugs_mirror':
    target_user => 'debbugs',
    collect_tag => 'bugs-mirror.debian.org',
  }

  # Note, services will not auto-reload or auto-start
  @@file { "/home/debbugs-mirror/.config/systemd/user/smartsync.service.wants/smartsync@${::fqdn}.service":
    ensure => link,
    tag    => 'bugs_mirror_to_bugs_mirror_master',
    target => '../smartsync@.service',
    owner  => 'debbugs-mirror',
    group  => 'debbugs-mirror',
  }
}
