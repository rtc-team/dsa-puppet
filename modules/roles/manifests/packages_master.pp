class roles::packages_master {
  include roles::packages

  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'packages.debian.org':
    owner => 'pkg_user',
    group => 'pkg_maint',
  }
}
