class roles::security_master {
  include roles::dakmaster
  include apache2

  exim::dkimdomain { 'ftp-master.debian.org': }

  ssl::service { 'security-master.debian.org':
    notify   => Exec['service apache2 reload'],
    key      => true,
    tlsaport => [443, 1873],
  }

  rsync::site { 'security_master':
    source      => 'puppet:///modules/roles/security_master/rsyncd.conf',
    # Needs to be at least twice the number of direct mirrors (currently 15) plus some spare
    max_clients => 50,
    sslname     => 'security-master.debian.org',
  }

  # export ssh allow rules for hosts that we should be able to access
  @@ferm::rule::simple { "dsa-ssh-from-security_master-${::fqdn}":
    tag         => 'ssh::server::from::security_master',
    description => 'Allow ssh access from security_master',
    chain       => 'ssh',
    saddr       => $base::public_addresses,
  }
}
