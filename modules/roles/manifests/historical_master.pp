class roles::historical_master {
  # export ssh allow rules for hosts that we should be able to access
  @@ferm::rule::simple { "dsa-ssh-from-historical_master-${::fqdn}":
    tag         => 'ssh::server::from::historical_master',
    description => 'Allow ssh access from historical-master',
    chain       => 'ssh',
    saddr       => $base::public_addresses,
  }
}
