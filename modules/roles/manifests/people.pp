# @param listen_addr IP addresses to have apache listen on port 443
class roles::people (
  Array[Stdlib::IP::Address] $listen_addr = [],
) {
  include apache2
  apache2::module { 'userdir': }
  ssl::service { 'people.debian.org': notify  => Exec['service apache2 reload'], key => true, }
  onion::service { 'people.debian.org': port => 80, target_address => 'people.debian.org', target_port => 80, direct => true }

  $ports = empty($listen_addr) ? {
    true => ['443'],
    default => enclose_ipv6($listen_addr).map |$a| { "${a}:443" },
  }
  file { '/etc/apache2/ports.conf':
    content => template('roles/apache-people-ports.conf.erb'),
  }

  $_enclosed_addresses = empty($listen_addr) ? {
    true => ['*'],
    default => enclose_ipv6($listen_addr),
  }
  $vhost_listen = $_enclosed_addresses.map |$a| { "${a}:443" } . join(' ')
  apache2::site { 'people.debian.org':
    site => 'people.debian.org.conf',
    content => template('roles/apache-people.debian.org.conf.erb'),
  }
}
