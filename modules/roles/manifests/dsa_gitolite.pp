# dsa's gitolite host
class roles::dsa_gitolite {
  # we push various things
  $key = $facts['git_key']

  if ($key) {
    ssh::authorized_key_add { 'dsa_gitolite::dsa_wiki_buildhost':
      target_user => 'dsa',
      command     => '/srv/dsa.debian.org/bin/update',
      key         => $facts['git_key'],
      collect_tag => 'dsa_wiki_buildhost',
    }

    ssh::authorized_key_add { 'dsa_gitolite::puppetmaster':
      target_user => 'dsa',
      command     => '/srv/puppet.debian.org/bin/update-git',
      key         => $facts['git_key'],
      collect_tag => 'puppetmaster',
    }


    ssh::authorized_key_add { 'dsa_gitolite::dns_primary_dnsadm':
      target_user => 'dnsadm',
      command     => '/srv/dns.debian.org/bin/from-adayevskaya',
      key         => $facts['git_key'],
      collect_tag => 'dns_primary',
    }

    ssh::authorized_key_add { 'dsa_gitolite::dns_primary_letsencrypt':
      target_user => 'letsencrypt',
      command     => '/srv/letsencrypt.debian.org/bin/from-adayevskaya',
      key         => $facts['git_key'],
      collect_tag => 'dns_primary',
    }
  }
}
