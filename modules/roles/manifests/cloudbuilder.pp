# basic setup to build cloud images
class roles::cloudbuilder {
  file { '/srv/cloudbuilder.debian.org':
    ensure => directory,
    owner  => 'cloud-build',
    group  => 'cloud-build',
    mode   => '2775',
  }
  file { '/home/cloud-build':
    ensure => link,
    target => '/srv/cloudbuilder.debian.org',
  }
}
