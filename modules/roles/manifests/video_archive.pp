# video.debian.net archive master
class roles::video_archive {
  package { 'git-annex': ensure => installed, }

  rsync::site { 'video.debian.net':
    source      => 'puppet:///modules/roles/video_archive/rsyncd.conf',
  }

  ssh::authorized_key_collect { 'video_archive_from_sreview':
    target_user => 'videoteam',
    collect_tag => 'sreview_to_video_archive',
  }
}
