class roles::i18n {
  include apache2
  ssl::service { 'i18n.debian.org': notify  => Exec['service apache2 reload'], key => true, }
}
