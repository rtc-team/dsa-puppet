class roles::anonscm {
	include apache2
	include apache2::rewrite

	ssl::service { 'anonscm.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
	apache2::site { '020-anonscm.debian.org':
		site   => 'anonscm.debian.org',
		content => template('roles/anonscm/anonscm.debian.org.conf.erb')
	}

	concat { '/etc/apache2/conf-available/anonscm.map.conf':
		notify  => Exec['service apache2 reload'],
	}
	concat::fragment { 'anonscm.map-header':
		target => '/etc/apache2/conf-available/anonscm.map.conf',
		order  => '000',
		content  => @(EOF)
			## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
			<Macro anonscm.debian.org-anonscm-map>
			| EOF
	}
	concat::fragment { 'anonscm.map-body':
		target => '/etc/apache2/conf-available/anonscm.map.conf',
		order  => '400',
		source => 'puppet:///modules/roles/anonscm/anonscm.map',
	}
	concat::fragment { 'anonscm.map-tail':
		target => '/etc/apache2/conf-available/anonscm.map.conf',
		order  => '900',
		content  => @(EOF)
			</Macro>
			| EOF
	}
	file { "/etc/apache2/conf-enabled/anonscm.map.conf":
		ensure => link,
		target => "../conf-available/anonscm.map.conf",
		notify  => Exec['service apache2 reload'],
	}

	file { '/srv/anonscm.debian.org':
		ensure  => directory,
	}

	file { '/srv/anonscm.debian.org/htdocs':
		ensure  => directory,
	}

	file { '/srv/anonscm.debian.org/htdocs/index.html':
		source => 'puppet:///modules/roles/anonscm/index.html',
	}
}
