# = Class: motd
#
# This class configures a sensible motd
#
# == Sample Usage:
#
#   include motd
#
class motd {
	file { '/etc/motd':
		notify  => undef,
		content => template('motd/motd.erb')
	}
}
