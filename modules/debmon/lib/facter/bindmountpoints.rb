# Get a list of mountpoints of bind mounts
Facter.add(:bindmountpoints) do
  confine kernel: :linux

  setcode do
    MI_RE = %r{(?<mountid>\d+) (?<parentid>\d+) (?<majorminor>\d+:\d+) (?<root>\S+) (?<mountpoint>\S+) (?<options>\S+) (?<optional>(?:\S+\s)+)?- (?<fstype>\S+) (?<mountsrc>\S+) (?<superopts>\S+)}
    bind_mounts = []
    File.open("/proc/self/mountinfo", "r") do |f|
      f.each_line do |line|
        matches = line.match(MI_RE)
        next if matches.nil?
        next if ["tmpfs", "devpts"].include?(matches[:fstype])
        next if matches[:mountpoint] =~ %r{^/run/schroot/.*}
        bind_mounts << matches[:mountpoint] if matches[:root] != '/'
      end
    end
    bind_mounts
  end
end
