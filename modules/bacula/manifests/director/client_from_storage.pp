# Bacula client config on the director
#
# This is stored config by a client, collected on the director
#
# @param client The name of the client (relevant for device names, media type names, etc.)
#
# @param storage_address         Address of the storage daemon
# @param port_sd                 Port of the storage daemon
# @param storage_secret          Shared secret between storage and director
# @param storage_device_name     Device name on the storage daemon for this node's backups
# @param storage_media_type_name Media type name on the storage daemon for this node's backupse
# @param volume_retention_full  how long to keep volumes with full backups
# @param volume_retention_diff  how long to keep volumes with differential backups
# @param volume_retention_inc   how long to keep volumes with incremental backups
define bacula::director::client_from_storage (
  Stdlib::Host $storage_address,
  Integer $port_sd,
  String $storage_secret,
  String $storage_device_name,
  String $storage_media_type_name,
  String $volume_retention_full = '100 days', # XXX remove defaults
  String $volume_retention_diff = '50 days', # XXX remove defaults
  String $volume_retention_inc  = '30 days', # XXX remove defaults

  Stdlib::Host $client = $name,
) {
  include bacula::director

  # For historical reasons, we use the same string for
  # director-internal storage name as we do for mediate
  # type names.  If we ever blow away the catalog and start
  # again, we should probably pick a different string here
  # (or there).
  $storage_name  = $storage_media_type_name

  # we define this in both bacula::director::client_from_storage and
  # bacula::director::client and it needs to match.
  $pool_name     = "${bacula::director::pool_name}-${client}"

  file { "/etc/bacula/conf.d/${client}_storage.conf":
    content => template('bacula/director/dir-per-client-from-storage.erb'),
    mode    => '0440',
    group   => bacula,
    notify  => Exec['bacula-director reload']
  }

  file { "/etc/bacula/storages-list.d/${client}.storage":
    content => "${storage_name}\n",
    mode    => '0440',
    group   => bacula,
    notify  => Exec['bacula-director reload']
  }
}

