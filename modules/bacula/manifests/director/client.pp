# Bacula client config on the director
#
# This is stored config by a client, collected on the director
#
# @param port_fd         port that this node's bacula-fd is listening on
# @param client          name of the client (relevant for device names, media type names, etc.)
# @param client_name     bacula client name
# @param client_secret   shared secret between director and client
# @param file_retention  how long to keep information about which files are in which volumes/jobs
# @param job_retention   how long to keep job records
define bacula::director::client (
  Integer $port_fd,
  Stdlib::Host $client = $name,
  String $client_name,
  String $client_secret,
  String $file_retention = '30 days', # XXX remove defaults
  String $job_retention = '100 days', # XXX remove defaults
) {
  include bacula::director

  # we define this in both bacula::director::client_from_storage and
  # bacula::director::client and it needs to match.
  $pool_name     = "${bacula::director::pool_name}-${client}"

  if defined(File["/etc/bacula/conf.d/${client}_storage.conf"]) {
    # this config is only valid if the pools defined in ${client}_storage exist.
    file { "/etc/bacula/conf.d/${client}.conf":
      content => template('bacula/director/dir-per-client.erb'),
      mode    => '0440',
      group   => bacula,
      notify  => Exec['bacula-director reload']
    }
  }
}

