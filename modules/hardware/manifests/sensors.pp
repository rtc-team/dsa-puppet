class hardware::sensors {
	if $::hw_can_temp_sensors or $::hw_can_fan_sensors {
		package { 'lm-sensors': ensure => installed, }
	} else {
		package { 'lm-sensors': ensure => purged, }
	}

	if $::hw_can_fan_sensors {
		munin::check { 'sensors_fan': script => 'sensors_' }
	} else {
		munin::check { 'sensors_fan': ensure => absent }
	}

	if $::hw_can_temp_sensors {
		munin::check { 'sensors_temp': script => 'sensors_' }
	} else {
		munin::check { 'sensors_temp': ensure => absent }
	}
}
