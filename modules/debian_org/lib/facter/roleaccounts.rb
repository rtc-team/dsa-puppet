begin
    require 'etc'

    %w{
       buildd
       dnsadm
       dsa
       geodnssync
       git
       letsencrypt
       portforwarder
       postgres
       }.each do |user|
        Facter.add("#{user}_user_exists") do
            setcode do
                result = false
                begin
                    if Etc.getpwnam(user)
                        result = true
                    end
                rescue ArgumentError
                end
                result
            end
        end

        Facter.add("#{user}_key") do
            setcode do
                key = nil
                begin
                    pwinfo = Etc.getpwnam(user)
                    if pwinfo and pwinfo.dir
                        keyfile = pwinfo.dir + '/.ssh/id_rsa.pub'
                        if FileTest.exist?(keyfile)
                            key = File.open(keyfile).read.chomp
                        end
                    end
                rescue ArgumentError
                end
                key
            end
        end
    end

rescue Exception => e
end
# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4:
