class debian_org::apt_restricted {
	base::aptrepo { 'db.debian.org.restricted':
		url        => 'https://db.debian.org/debian-admin',
		suite      => "${::lsbdistcodename}-restricted",
		key        => 'puppet:///modules/debian_org/db.debian.org.gpg',
		components => 'non-free',
	}

	@@concat::fragment { "debian_org::apt_restricted::apache-acl::host::${::fqdn}":
		tag        => 'debian_org::apt_restricted::apache-acl',
		target     => '/etc/apache2/conf-available/puppet-restricted-acl.conf',
		content    => @("EOF"),
			# ${::fqdn}
			Require ip ${ $base::public_addresses.join(' ') }
			| EOF
	}
}
