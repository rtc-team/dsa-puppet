# == Class: debian_org
#
# Stuff common to all debian.org servers
#
class debian_org::apt {
  $mirror = lookup('apt::sources::debian::location')

  base::aptrepo { 'debian':
    url        => $mirror,
    suite      => [ $::lsbdistcodename, "${::lsbdistcodename}-backports", "${::lsbdistcodename}-updates" ],
    components => ['main','contrib','non-free']
  }

  if versioncmp($::lsbmajdistrelease, '10') <= 0 {
    base::aptrepo { 'security':
      url        => [ 'https://deb.debian.org/debian-security' ],
      suite      => "${::lsbdistcodename}/updates",
      components => ['main','contrib','non-free']
    }
  } else {
    base::aptrepo { 'security':
      url        => [ 'https://deb.debian.org/debian-security' ],
      suite      => "${::lsbdistcodename}-security",
      components => ['main','contrib','non-free']
    }
  }


  # ca-certificates is installed by the ssl module
  if versioncmp($::lsbmajdistrelease, '9') <= 0 {
    package { 'apt-transport-https':
      ensure => installed,
    }
  } else {
    # transitional package in buster
    package { 'apt-transport-https':
      ensure => purged,
    }
  }
  $dbdosuites = [ 'debian-all', $::lsbdistcodename ]
  base::aptrepo { 'db.debian.org':
    url        => 'https://db.debian.org/debian-admin',
    suite      => $dbdosuites,
    components => 'main',
    key        => 'puppet:///modules/debian_org/db.debian.org.gpg',
  }

  if $::hostname in [] {
    $pu_ensure = 'present'
  } else {
    $pu_ensure = 'absent'
  }

  base::aptrepo { 'proposed-updates':
    ensure     => $pu_ensure,
    url        => $mirror,
    suite      => "${::lsbdistcodename}-proposed-updates",
    components => ['main','contrib','non-free']
  }



  file { '/etc/apt/trusted-keys.d':
    ensure => absent,
    force  => true,
  }

  file { '/etc/apt/trusted.gpg':
    mode    => '0600',
    content => '',
  }

  file { '/etc/apt/preferences':
    source => 'puppet:///modules/debian_org/apt.preferences',
  }
  file { '/etc/apt/apt.conf.d/local-compression':
    source => 'puppet:///modules/debian_org/apt.conf.d/local-compression',
  }
  file { '/etc/apt/apt.conf.d/local-recommends':
    source => 'puppet:///modules/debian_org/apt.conf.d/local-recommends',
  }
  file { '/etc/apt/apt.conf.d/local-pdiffs':
    source => 'puppet:///modules/debian_org/apt.conf.d/local-pdiffs',
  }
  file { '/etc/apt/apt.conf.d/local-langs':
    source => 'puppet:///modules/debian_org/apt.conf.d/local-langs',
  }
  file { '/etc/apt/apt.conf.d/local-cainfo':
    source => 'puppet:///modules/debian_org/apt.conf.d/local-cainfo',
  }
  file { '/etc/apt/apt.conf.d/local-pkglist':
    ensure => 'absent',
  }

  exec { 'apt-get update':
    path    => '/usr/bin:/usr/sbin:/bin:/sbin',
    onlyif  => '/usr/local/bin/check_for_updates',
    require => File['/usr/local/bin/check_for_updates']
  }
  Exec['apt-get update']->Package<| tag == extra_repo |>
}
