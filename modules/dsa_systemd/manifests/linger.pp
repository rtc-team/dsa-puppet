# enable (or disable) lingering for a user
define dsa_systemd::linger(
  Enum['present','absent'] $ensure = 'present',
) {
  include dsa_systemd

  file { "/var/lib/systemd/linger/${name}":
    ensure => $ensure,
  }
}
