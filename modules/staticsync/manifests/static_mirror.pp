# a static mirror.  It will get content for all components that it servers
# from each component's master.
#
# Static mirrors register themselves via a stored-conf /etc/static-clients.conf
# that the masters collect.  Mirrors can always pull, but they only get
# triggered if they are in /etc/static-clients.conf.  So during initial
# setup it might make sense to not get triggered and thus block updates of
# components until all mirrors are in sync; set $get_triggered = false for that.
#
class staticsync::static_mirror (
  Boolean $get_triggered = true,
) {
  include staticsync::base
  include staticsync::srvdir

  # mirrors talk only to masters
  class { 'staticsync::ssh':
    add_tag     => 'staticsync-master',
    collect_tag => 'staticsync-mirror',
  }

  file { '/usr/local/bin/static-mirror-run':
    source => 'puppet:///modules/staticsync/static-mirror-run',
    mode   => '0555',
  }
  file { '/usr/local/bin/static-mirror-run-all':
    source => 'puppet:///modules/staticsync/static-mirror-run-all',
    mode   => '0555',
  }
  file { '/etc/cron.d/puppet-static-mirror': ensure => absent, }
  concat::fragment { 'puppet-crontab--static-mirror':
    target  => '/etc/cron.d/puppet-crontab',
    content => @("EOF"),
        @reboot ${staticsync::user} sleep 60; chronic static-mirror-run-all
        | EOF
  }

  if $get_triggered {
    @@concat::fragment { "static-client-${::fqdn}":
      tag     => 'staticsync::static-mirrors-to-trigger',
      content => $::fqdn,
      target  => '/etc/static-clients.conf',
    }
  }
}
