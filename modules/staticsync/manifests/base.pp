# the base class defining things common for all three static classes (master, mirror, source)
class staticsync::base inherits staticsync {

  # fetch the list of static mirrors from PuppetDB
  #
  # This list is necessary so we can both do "include only these
  # mirrors" but also "exclude these mirrors and include all others"
  # in the config.
  $query = 'nodes[certname] { resources { type = "Class" and title = "Staticsync::Static_mirror" } }'
  $static_mirrors = sort(puppetdb_query($query).map |$value| { $value["certname"] })

  file { '/etc/static-components.conf':
    content => template('staticsync/static-components.conf.erb'),
  }

  file { '/usr/local/bin/staticsync-ssh-wrap':
    source => 'puppet:///modules/staticsync/staticsync-ssh-wrap',
    mode   => '0555',
  }

  file { '/usr/local/bin/static-update-component':
    source => 'puppet:///modules/staticsync/static-update-component',
    mode   => '0555',
  }

  file { '/etc/staticsync.conf':
    content  => @("EOF"),
        # This file is sourced by bash
        # and parsed by python
        #  - empty lines and lines starting with a # are ignored.
        #  - other lines are key=value.  No extra spaces anywhere.  No quoting.
        base=${staticsync::basedir}
        masterbase=${staticsync::basedir}/master
        staticuser=${staticsync::user}
        | EOF
  }
}
